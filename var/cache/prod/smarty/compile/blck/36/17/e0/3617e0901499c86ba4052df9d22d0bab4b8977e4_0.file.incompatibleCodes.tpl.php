<?php
/* Smarty version 3.1.43, created on 2022-12-06 21:37:08
  from 'C:\laragon\www\prestashop\modules\ps_checkout\views\templates\hook\adminAfterHeader\incompatibleCodes.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_638fa7f45de6f7_57027466',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3617e0901499c86ba4052df9d22d0bab4b8977e4' => 
    array (
      0 => 'C:\\laragon\\www\\prestashop\\modules\\ps_checkout\\views\\templates\\hook\\adminAfterHeader\\incompatibleCodes.tpl',
      1 => 1669021948,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_638fa7f45de6f7_57027466 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['incompatibleCodes']->value) {?>
    <div class="container">
        <?php if ($_smarty_tpl->tpl_vars['isShop17']->value) {?>
            <div class="banner-alert">
                <div class="banner-icon">
                    <i class="material-icons">error_outline</i>
                </div>
        <?php } else { ?>
            <div class="alert alert-warning">
        <?php }?>

            <div class="banner-text">
                <h2>
                  <?php if ($_smarty_tpl->tpl_vars['codesType']->value === 'countries') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'PrestaShop Checkout transactions won\'t work in some of your configured countries, but there is a solution !','mod'=>'ps_checkout'),$_smarty_tpl ) );?>

                  <?php } elseif ($_smarty_tpl->tpl_vars['codesType']->value === 'currencies') {?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'PrestaShop Checkout transactions won\'t work in some of your configured currencies, but there is a solution !','mod'=>'ps_checkout'),$_smarty_tpl ) );?>

                  <?php }?>
                </h2>

                <p class="banner-upgrade-info">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please upgrade your settings for :','mod'=>'ps_checkout'),$_smarty_tpl ) );?>

                </p>

                <p class="incompatible-list">
                    <b><i>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['incompatibleCodes']->value, 'incompatibleCode', false, 'key');
$_smarty_tpl->tpl_vars['incompatibleCode']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['incompatibleCode']->value) {
$_smarty_tpl->tpl_vars['incompatibleCode']->do_else = false;
?>
                            <?php echo $_smarty_tpl->tpl_vars['incompatibleCode']->value;
if ($_smarty_tpl->tpl_vars['key']->value != count($_smarty_tpl->tpl_vars['incompatibleCodes']->value)-1) {?>,<?php }?>
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </i></b>
                </p>

                <a href="<?php echo $_smarty_tpl->tpl_vars['paymentPreferencesLink']->value;?>
" class="button-link" target="_blank">
                    <?php if ($_smarty_tpl->tpl_vars['codesType']->value === 'countries') {?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Change countries activation for this payment module','mod'=>'ps_checkout'),$_smarty_tpl ) );?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['codesType']->value === 'currencies') {?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Change currencies activation for this payment module','mod'=>'ps_checkout'),$_smarty_tpl ) );?>

                    <?php }?>
                </a>

                <a class="btn btn-link banner-link" href="<?php echo $_smarty_tpl->tpl_vars['paypalLink']->value;?>
" target="_blank">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Know more about compliant ISO Codes','mod'=>'ps_checkout'),$_smarty_tpl ) );?>


                    <?php if ($_smarty_tpl->tpl_vars['isShop17']->value) {?>
                        <i class="material-icons banner-link-icon">trending_flat</i>
                    <?php } else { ?>
                        <i class="icon-long-arrow-right"></i>
                    <?php }?>
                </a>
            </div>
        </div>
    </div>
<?php }
}
}
