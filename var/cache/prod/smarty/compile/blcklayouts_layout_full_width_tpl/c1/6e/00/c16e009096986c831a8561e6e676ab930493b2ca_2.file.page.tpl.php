<?php
/* Smarty version 3.1.43, created on 2022-12-07 10:40:55
  from 'C:\laragon\www\prestashop\themes\blck\templates\customer\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_63905fa75e5f08_83938578',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c16e009096986c831a8561e6e676ab930493b2ca' => 
    array (
      0 => 'C:\\laragon\\www\\prestashop\\themes\\blck\\templates\\customer\\page.tpl',
      1 => 1669038712,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/notifications.tpl' => 1,
    'file:customer/_partials/my-account-links.tpl' => 1,
  ),
),false)) {
function content_63905fa75e5f08_83938578 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_56206321063905fa754c4a8_04965742', 'notifications');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_127801765663905fa754d8b0_89474498', 'page_content_container');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_110537929563905fa75e2bc8_97444060', 'page_footer_container');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'notifications'} */
class Block_56206321063905fa754c4a8_04965742 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'notifications' => 
  array (
    0 => 'Block_56206321063905fa754c4a8_04965742',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'notifications'} */
/* {block 'pageContentClass'} */
class Block_26533833263905fa754e2f6_76451606 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
page-content <?php
}
}
/* {/block 'pageContentClass'} */
/* {block 'customer_notifications'} */
class Block_93759063363905fa7550c00_55732703 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'customer_notifications'} */
/* {block 'page_content_top'} */
class Block_173334559363905fa75502c6_40535820 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_93759063363905fa7550c00_55732703', 'customer_notifications', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_163963553663905fa75e1bc2_91430859 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <!-- Page content -->
    <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_127801765663905fa754d8b0_89474498 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_127801765663905fa754d8b0_89474498',
  ),
  'pageContentClass' => 
  array (
    0 => 'Block_26533833263905fa754e2f6_76451606',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_173334559363905fa75502c6_40535820',
  ),
  'customer_notifications' => 
  array (
    0 => 'Block_93759063363905fa7550c00_55732703',
  ),
  'page_content' => 
  array (
    0 => 'Block_163963553663905fa75e1bc2_91430859',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <section id="content" class="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_26533833263905fa754e2f6_76451606', 'pageContentClass', $this->tplIndex);
?>
page-content--<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_173334559363905fa75502c6_40535820', 'page_content_top', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_163963553663905fa75e1bc2_91430859', 'page_content', $this->tplIndex);
?>

  </section>
<?php
}
}
/* {/block 'page_content_container'} */
/* {block 'pageFooterClass'} */
class Block_85363606563905fa75e31c7_33024954 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
page-footer <?php
}
}
/* {/block 'pageFooterClass'} */
/* {block 'my_account_links'} */
class Block_18261631663905fa75e4909_89521862 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

              <?php $_smarty_tpl->_subTemplateRender('file:customer/_partials/my-account-links.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
          <?php
}
}
/* {/block 'my_account_links'} */
/* {block 'page_footer'} */
class Block_153955405163905fa75e4376_14837015 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18261631663905fa75e4909_89521862', 'my_account_links', $this->tplIndex);
?>

      <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_110537929563905fa75e2bc8_97444060 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_footer_container' => 
  array (
    0 => 'Block_110537929563905fa75e2bc8_97444060',
  ),
  'pageFooterClass' => 
  array (
    0 => 'Block_85363606563905fa75e31c7_33024954',
  ),
  'page_footer' => 
  array (
    0 => 'Block_153955405163905fa75e4376_14837015',
  ),
  'my_account_links' => 
  array (
    0 => 'Block_18261631663905fa75e4909_89521862',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <footer class="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_85363606563905fa75e31c7_33024954', 'pageFooterClass', $this->tplIndex);
?>
page-footer--<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_153955405163905fa75e4376_14837015', 'page_footer', $this->tplIndex);
?>

  </footer>
<?php
}
}
/* {/block 'page_footer_container'} */
}
