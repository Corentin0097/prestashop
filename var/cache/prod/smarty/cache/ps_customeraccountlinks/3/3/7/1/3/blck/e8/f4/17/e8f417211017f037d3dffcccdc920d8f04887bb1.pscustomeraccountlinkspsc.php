<?php
/* Smarty version 3.1.43, created on 2022-12-06 20:57:59
  from 'module:pscustomeraccountlinkspsc' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_638f9ec73024e0_66118175',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '42f9461127ce7396a601c2484841253ea5ba658f' => 
    array (
      0 => 'module:pscustomeraccountlinkspsc',
      1 => 1669038712,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_638f9ec73024e0_66118175 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>
<div id="block_myaccount_infos" class="col-lg-3 links wrapper">
    <p class="footer__title footer__title--desktop">Il tuo account</p>
    <a href="#footer_account_list" class="footer__title--mobile footer__title" data-toggle="collapse">Il tuo account</a>
  <ul class="account-list collapse show" data-collapse-hide-mobile id="footer_account_list">
            <li>
          <a href="http://localhost/prestashop/it/dati-personali" title="Informazioni personali" rel="nofollow">
            Informazioni personali
          </a>
        </li>
            <li>
          <a href="http://localhost/prestashop/it/cronologia-ordini" title="Ordini" rel="nofollow">
            Ordini
          </a>
        </li>
            <li>
          <a href="http://localhost/prestashop/it/buono-ordine" title="Note di credito" rel="nofollow">
            Note di credito
          </a>
        </li>
            <li>
          <a href="http://localhost/prestashop/it/indirizzi" title="Indirizzi" rel="nofollow">
            Indirizzi
          </a>
        </li>
            <li>
          <a href="http://localhost/prestashop/it/buoni-sconto" title="Buoni" rel="nofollow">
            Buoni
          </a>
        </li>
          <li>
    <a href="http://localhost/prestashop/it/module/blockwishlist/lists" title="My wishlists" rel="nofollow">
      Lista dei desideri
    </a>
  </li>

	</ul>
</div>
<?php }
}
