<?php
/* Smarty version 3.1.43, created on 2022-12-06 21:08:08
  from 'module:pslinklistviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_638fa128b84053_35437212',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:pslinklistviewstemplatesh',
      1 => 1669038712,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_638fa128b84053_35437212 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>
      <div class="col-lg-3">
              <p class="footer__title footer__title--desktop">Produits</p>
        <a href="#footer_sub_menu_36505" class="footer__title--mobile footer__title" data-toggle="collapse">Produits</a>
      <ul id="footer_sub_menu_36505" class="collapse show" data-collapse-hide-mobile>
                  <li>
            <a
                id="link-category-12-1"
                class="category-link"
                href="http://localhost/prestashop/fr/12-enfant"
                title="Vêtements pour enfants"
                            >
              Enfant
            </a>
          </li>
                  <li>
            <a
                id="link-category-10-1"
                class="category-link"
                href="http://localhost/prestashop/fr/10-femme"
                title="Vêtements pour femmes"
                            >
              Femme
            </a>
          </li>
                  <li>
            <a
                id="link-category-11-1"
                class="category-link"
                href="http://localhost/prestashop/fr/11-homme"
                title="Vêtements pour hommes"
                            >
              Homme
            </a>
          </li>
              </ul>
    </div>
      <div class="col-lg-3">
              <p class="footer__title footer__title--desktop">À Propos</p>
        <a href="#footer_sub_menu_66540" class="footer__title--mobile footer__title" data-toggle="collapse">À Propos</a>
      <ul id="footer_sub_menu_66540" class="collapse show" data-collapse-hide-mobile>
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/content/1-livraison"
                title="Nos conditions de livraison"
                            >
              Livraison
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/content/2-mentions-legales"
                title="Mentions légales"
                            >
              Mentions légales
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/content/3-conditions-utilisation"
                title="Nos conditions d&#039;utilisation"
                            >
              Conditions d&#039;utilisation
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/content/4-a-propos"
                title="En savoir plus sur notre entreprise"
                            >
              A propos
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/content/5-paiement-securise"
                title="Notre méthode de paiement sécurisé"
                            >
              Paiement sécurisé
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://localhost/prestashop/fr/nous-contacter"
                title="Utiliser le formulaire pour nous contacter"
                            >
              Contactez-nous
            </a>
          </li>
              </ul>
    </div>
  
<?php }
}
