<?php
/* Smarty version 3.1.43, created on 2022-12-06 21:05:54
  from 'module:pslinklistviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_638fa0a254abc8_80585148',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:pslinklistviewstemplatesh',
      1 => 1669038712,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_638fa0a254abc8_80585148 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>
      <div class="col-lg-3">
              <p class="footer__title footer__title--desktop">Products</p>
        <a href="#footer_sub_menu_32814" class="footer__title--mobile footer__title" data-toggle="collapse">Products</a>
      <ul id="footer_sub_menu_32814" class="collapse show" data-collapse-hide-mobile>
                  <li>
            <a
                id="link-category-12-1"
                class="category-link"
                href="http://localhost/prestashop/en/12-kid"
                title="Clothes for kids"
                            >
              Kid
            </a>
          </li>
                  <li>
            <a
                id="link-category-10-1"
                class="category-link"
                href="http://localhost/prestashop/en/10-woman"
                title="Clothes for women"
                            >
              Woman
            </a>
          </li>
                  <li>
            <a
                id="link-category-11-1"
                class="category-link"
                href="http://localhost/prestashop/en/11-man"
                title="Clothes for men"
                            >
              Man
            </a>
          </li>
              </ul>
    </div>
      <div class="col-lg-3">
              <p class="footer__title footer__title--desktop">Our company</p>
        <a href="#footer_sub_menu_56240" class="footer__title--mobile footer__title" data-toggle="collapse">Our company</a>
      <ul id="footer_sub_menu_56240" class="collapse show" data-collapse-hide-mobile>
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://localhost/prestashop/en/content/1-delivery"
                title="Our terms and conditions of delivery"
                            >
              Delivery
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://localhost/prestashop/en/content/2-legal-notice"
                title="Legal notice"
                            >
              Legal Notice
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://localhost/prestashop/en/content/3-terms-and-conditions-of-use"
                title="Our terms and conditions of use"
                            >
              Terms and conditions of use
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://localhost/prestashop/en/content/4-about-us"
                title="Learn more about us"
                            >
              About us
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://localhost/prestashop/en/content/5-secure-payment"
                title="Our secure payment method"
                            >
              Secure payment
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://localhost/prestashop/en/contact-us"
                title="Use our form to contact us"
                            >
              Contact us
            </a>
          </li>
              </ul>
    </div>
  
<?php }
}
