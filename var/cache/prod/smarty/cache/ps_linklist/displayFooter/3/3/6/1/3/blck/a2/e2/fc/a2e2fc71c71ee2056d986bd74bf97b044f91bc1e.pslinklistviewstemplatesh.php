<?php
/* Smarty version 3.1.43, created on 2022-12-06 21:07:54
  from 'module:pslinklistviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_638fa11a59c616_91909753',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:pslinklistviewstemplatesh',
      1 => 1669038712,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_638fa11a59c616_91909753 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>
      <div class="col-lg-3">
              <p class="footer__title footer__title--desktop">Productos</p>
        <a href="#footer_sub_menu_19344" class="footer__title--mobile footer__title" data-toggle="collapse">Productos</a>
      <ul id="footer_sub_menu_19344" class="collapse show" data-collapse-hide-mobile>
                  <li>
            <a
                id="link-category-12-1"
                class="category-link"
                href="http://localhost/prestashop/es/12-ninos"
                title="Ropa para niños"
                            >
              Niños
            </a>
          </li>
                  <li>
            <a
                id="link-category-10-1"
                class="category-link"
                href="http://localhost/prestashop/es/10-mujer"
                title="Ropa de mujer"
                            >
              Mujer
            </a>
          </li>
                  <li>
            <a
                id="link-category-11-1"
                class="category-link"
                href="http://localhost/prestashop/es/11-hombre"
                title="Ropa de hombre"
                            >
              Hombre
            </a>
          </li>
              </ul>
    </div>
      <div class="col-lg-3">
              <p class="footer__title footer__title--desktop">Nuestra empresa</p>
        <a href="#footer_sub_menu_26103" class="footer__title--mobile footer__title" data-toggle="collapse">Nuestra empresa</a>
      <ul id="footer_sub_menu_26103" class="collapse show" data-collapse-hide-mobile>
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://localhost/prestashop/es/content/1-delivery"
                title="Our terms and conditions of delivery"
                            >
              Delivery
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://localhost/prestashop/es/content/2-legal-notice"
                title="Legal notice"
                            >
              Legal Notice
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://localhost/prestashop/es/content/3-terms-and-conditions-of-use"
                title="Our terms and conditions of use"
                            >
              Terms and conditions of use
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://localhost/prestashop/es/content/4-about-us"
                title="Learn more about us"
                            >
              About us
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://localhost/prestashop/es/content/5-secure-payment"
                title="Our secure payment method"
                            >
              Secure payment
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://localhost/prestashop/es/contactenos"
                title="Contáctenos"
                            >
              Contacte con nosotros
            </a>
          </li>
              </ul>
    </div>
  
<?php }
}
