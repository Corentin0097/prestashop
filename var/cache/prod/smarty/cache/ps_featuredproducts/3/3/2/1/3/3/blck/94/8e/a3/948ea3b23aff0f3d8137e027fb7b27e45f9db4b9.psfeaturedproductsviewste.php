<?php
/* Smarty version 3.1.43, created on 2022-12-06 21:26:40
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_638fa580413ba2_75368134',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1669038712,
      2 => 'module',
    ),
    'd1d136c1d858bf6e0645ae4c8b208feb39ba0487' => 
    array (
      0 => 'C:\\laragon\\www\\prestashop\\themes\\blck\\templates\\catalog\\_partials\\miniatures\\product.tpl',
      1 => 1669038712,
      2 => 'file',
    ),
    '5f8803f1488e94f60ec67b38d7f5531dc9c9924f' => 
    array (
      0 => 'C:\\laragon\\www\\prestashop\\themes\\blck\\templates\\catalog\\_partials\\variant-links.tpl',
      1 => 1669038712,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_638fa580413ba2_75368134 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <p class="products-section-title">
    Produits populaires
  </p>
    <div class="products products-slick spacing-md-top products--slickmobile" data-slick='{"slidesToShow": 1,"slidesToScroll": 1,"mobileFirst":true,"arrows":true,"rows":0,"responsive": [{"breakpoint": 992,"settings":{"arrows":true,"slidesToShow": 4,"slidesToScroll": 4,"arrows":true}},{"breakpoint": 720,"settings":{"arrows":true,"slidesToShow": 3,"slidesToScroll": 3}},{"breakpoint": 540,"settings":{"arrows":true,"slidesToShow": 2,"slidesToScroll": 2}}]}'>
          
    <article class="product-miniature js-product-miniature mb-3" data-id-product="27" data-id-product-attribute="42">
        <div class="card card-product">

            <div class="card-img-top product__card-img">
                
                    <a href="http://localhost/prestashop/fr/accueil/27-42-t-shirt-h.html#/1-taille-s/11-couleur-noir" class="thumbnail product-thumbnail rc ratio1_1">
                                                    <img
                                    data-src = "http://localhost/prestashop/38-home_default/t-shirt-h.jpg"
                                    alt = "T-shirt"
                                    data-full-size-image-url = "http://localhost/prestashop/38-large_default/t-shirt-h.jpg"
                                    class="lazyload"
                            >
                                            </a>
                
                <div class="highlighted-informations text-center p-2 visible--desktop">
                    
                        <span class="quick-view" data-link-action="quickview">
                      <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
                  </span>
                    

                    
                                                    <div class="variant-links">
                        <a href="http://localhost/prestashop/fr/accueil/27-46-t-shirt-h.html#/1-taille-s/8-couleur-blanc"
               class="color mx-1"
               title="Blanc"
                                         style="background-color: #ffffff"                                 ><span class="sr-only">Blanc</span></a>
                                <a href="http://localhost/prestashop/fr/accueil/27-42-t-shirt-h.html#/1-taille-s/11-couleur-noir"
               class="color mx-1"
               title="Noir"
                                         style="background-color: #434A54"                                 ><span class="sr-only">Noir</span></a>
                </div>
                                            
                </div>
            </div>
            
            <div class="card-body">
                <div class="product-description product__card-desc">
                    
                                                    <p class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/27-42-t-shirt-h.html#/1-taille-s/11-couleur-noir">T-shirt</a></p>
                                            
                    
                        


                    
                    
                                                    <div class="product-price-and-shipping text-center">
                                
                                

                                <span class="sr-only">Prix</span>
                                <span class="price">14,52 €</span>


                                

                                

                                <form class="pl_atc mt-2" action="http://localhost/prestashop/fr/panier" method="post">
                                   <input type="hidden" name="token" value="c52f0980ee6add53ed1c1d8408b340f7">
                                   <input type="hidden" value="27" name="id_product">
                                   <input type="number" class="input-group form-control" name="qty" value="1">
                                   <button data-button-action="add-to-cart"  class="btn btn-primary">Ajouter au panier</button>
                                </form>
                            </div>
                                            


                </div>

            </div>
                        
                <ul class="product-flags">
                                                        </ul>
            
        </div>
        


    </article>

          
    <article class="product-miniature js-product-miniature mb-3" data-id-product="28" data-id-product-attribute="54">
        <div class="card card-product">

            <div class="card-img-top product__card-img">
                
                    <a href="http://localhost/prestashop/fr/accueil/28-54-chemise.html#/1-taille-s/11-couleur-noir" class="thumbnail product-thumbnail rc ratio1_1">
                                                    <img
                                    data-src = "http://localhost/prestashop/42-home_default/chemise.jpg"
                                    alt = "Chemise"
                                    data-full-size-image-url = "http://localhost/prestashop/42-large_default/chemise.jpg"
                                    class="lazyload"
                            >
                                            </a>
                
                <div class="highlighted-informations text-center p-2 visible--desktop">
                    
                        <span class="quick-view" data-link-action="quickview">
                      <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
                  </span>
                    

                    
                                                    <div class="variant-links">
                        <a href="http://localhost/prestashop/fr/accueil/28-50-chemise.html#/1-taille-s/8-couleur-blanc"
               class="color mx-1"
               title="Blanc"
                                         style="background-color: #ffffff"                                 ><span class="sr-only">Blanc</span></a>
                                <a href="http://localhost/prestashop/fr/accueil/28-54-chemise.html#/1-taille-s/11-couleur-noir"
               class="color mx-1"
               title="Noir"
                                         style="background-color: #434A54"                                 ><span class="sr-only">Noir</span></a>
                </div>
                                            
                </div>
            </div>
            
            <div class="card-body">
                <div class="product-description product__card-desc">
                    
                                                    <p class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/28-54-chemise.html#/1-taille-s/11-couleur-noir">Chemise</a></p>
                                            
                    
                        


                    
                    
                                                    <div class="product-price-and-shipping text-center">
                                
                                

                                <span class="sr-only">Prix</span>
                                <span class="price">18,15 €</span>


                                

                                

                                <form class="pl_atc mt-2" action="http://localhost/prestashop/fr/panier" method="post">
                                   <input type="hidden" name="token" value="c52f0980ee6add53ed1c1d8408b340f7">
                                   <input type="hidden" value="28" name="id_product">
                                   <input type="number" class="input-group form-control" name="qty" value="1">
                                   <button data-button-action="add-to-cart"  class="btn btn-primary">Ajouter au panier</button>
                                </form>
                            </div>
                                            


                </div>

            </div>
                        
                <ul class="product-flags">
                                                        </ul>
            
        </div>
        


    </article>

          
    <article class="product-miniature js-product-miniature mb-3" data-id-product="25" data-id-product-attribute="70">
        <div class="card card-product">

            <div class="card-img-top product__card-img">
                
                    <a href="http://localhost/prestashop/fr/accueil/25-70-sweat-h.html#/1-taille-s/11-couleur-noir" class="thumbnail product-thumbnail rc ratio1_1">
                                                    <img
                                    data-src = "http://localhost/prestashop/29-home_default/sweat-h.jpg"
                                    alt = "Sweat à capuche"
                                    data-full-size-image-url = "http://localhost/prestashop/29-large_default/sweat-h.jpg"
                                    class="lazyload"
                            >
                                            </a>
                
                <div class="highlighted-informations text-center p-2 visible--desktop">
                    
                        <span class="quick-view" data-link-action="quickview">
                      <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
                  </span>
                    

                    
                                                    <div class="variant-links">
                        <a href="http://localhost/prestashop/fr/accueil/25-66-sweat-h.html#/1-taille-s/5-couleur-gris"
               class="color mx-1"
               title="Gris"
                                         style="background-color: #AAB2BD"                                 ><span class="sr-only">Gris</span></a>
                                <a href="http://localhost/prestashop/fr/accueil/25-70-sweat-h.html#/1-taille-s/11-couleur-noir"
               class="color mx-1"
               title="Noir"
                                         style="background-color: #434A54"                                 ><span class="sr-only">Noir</span></a>
                </div>
                                            
                </div>
            </div>
            
            <div class="card-body">
                <div class="product-description product__card-desc">
                    
                                                    <p class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/25-70-sweat-h.html#/1-taille-s/11-couleur-noir">Sweat à capuche</a></p>
                                            
                    
                        


                    
                    
                                                    <div class="product-price-and-shipping text-center">
                                
                                

                                <span class="sr-only">Prix</span>
                                <span class="price">30,25 €</span>


                                

                                

                                <form class="pl_atc mt-2" action="http://localhost/prestashop/fr/panier" method="post">
                                   <input type="hidden" name="token" value="c52f0980ee6add53ed1c1d8408b340f7">
                                   <input type="hidden" value="25" name="id_product">
                                   <input type="number" class="input-group form-control" name="qty" value="1">
                                   <button data-button-action="add-to-cart"  class="btn btn-primary">Ajouter au panier</button>
                                </form>
                            </div>
                                            


                </div>

            </div>
                        
                <ul class="product-flags">
                                                        </ul>
            
        </div>
        


    </article>

          
    <article class="product-miniature js-product-miniature mb-3" data-id-product="37" data-id-product-attribute="140">
        <div class="card card-product">

            <div class="card-img-top product__card-img">
                
                    <a href="http://localhost/prestashop/fr/accueil/37-140-sweat-a-capuche.html#/1-taille-s/11-couleur-noir" class="thumbnail product-thumbnail rc ratio1_1">
                                                    <img
                                    data-src = "http://localhost/prestashop/86-home_default/sweat-a-capuche.jpg"
                                    alt = "Sweat à capuche"
                                    data-full-size-image-url = "http://localhost/prestashop/86-large_default/sweat-a-capuche.jpg"
                                    class="lazyload"
                            >
                                            </a>
                
                <div class="highlighted-informations text-center p-2 visible--desktop">
                    
                        <span class="quick-view" data-link-action="quickview">
                      <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
                  </span>
                    

                    
                                                    <div class="variant-links">
                        <a href="http://localhost/prestashop/fr/accueil/37-136-sweat-a-capuche.html#/1-taille-s/5-couleur-gris"
               class="color mx-1"
               title="Gris"
                                         style="background-color: #AAB2BD"                                 ><span class="sr-only">Gris</span></a>
                                <a href="http://localhost/prestashop/fr/accueil/37-140-sweat-a-capuche.html#/1-taille-s/11-couleur-noir"
               class="color mx-1"
               title="Noir"
                                         style="background-color: #434A54"                                 ><span class="sr-only">Noir</span></a>
                </div>
                                            
                </div>
            </div>
            
            <div class="card-body">
                <div class="product-description product__card-desc">
                    
                                                    <p class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/37-140-sweat-a-capuche.html#/1-taille-s/11-couleur-noir">Sweat à capuche</a></p>
                                            
                    
                        


                    
                    
                                                    <div class="product-price-and-shipping text-center">
                                
                                

                                <span class="sr-only">Prix</span>
                                <span class="price">26,62 €</span>


                                

                                

                                <form class="pl_atc mt-2" action="http://localhost/prestashop/fr/panier" method="post">
                                   <input type="hidden" name="token" value="c52f0980ee6add53ed1c1d8408b340f7">
                                   <input type="hidden" value="37" name="id_product">
                                   <input type="number" class="input-group form-control" name="qty" value="1">
                                   <button data-button-action="add-to-cart"  class="btn btn-primary">Ajouter au panier</button>
                                </form>
                            </div>
                                            


                </div>

            </div>
                        
                <ul class="product-flags">
                                                        </ul>
            
        </div>
        


    </article>

          
    <article class="product-miniature js-product-miniature mb-3" data-id-product="35" data-id-product-attribute="124">
        <div class="card card-product">

            <div class="card-img-top product__card-img">
                
                    <a href="http://localhost/prestashop/fr/accueil/35-124-polo.html#/1-taille-s/11-couleur-noir" class="thumbnail product-thumbnail rc ratio1_1">
                                                    <img
                                    data-src = "http://localhost/prestashop/78-home_default/polo.jpg"
                                    alt = "Polo"
                                    data-full-size-image-url = "http://localhost/prestashop/78-large_default/polo.jpg"
                                    class="lazyload"
                            >
                                            </a>
                
                <div class="highlighted-informations text-center p-2 visible--desktop">
                    
                        <span class="quick-view" data-link-action="quickview">
                      <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
                  </span>
                    

                    
                                                    <div class="variant-links">
                        <a href="http://localhost/prestashop/fr/accueil/35-120-polo.html#/1-taille-s/8-couleur-blanc"
               class="color mx-1"
               title="Blanc"
                                         style="background-color: #ffffff"                                 ><span class="sr-only">Blanc</span></a>
                                <a href="http://localhost/prestashop/fr/accueil/35-124-polo.html#/1-taille-s/11-couleur-noir"
               class="color mx-1"
               title="Noir"
                                         style="background-color: #434A54"                                 ><span class="sr-only">Noir</span></a>
                </div>
                                            
                </div>
            </div>
            
            <div class="card-body">
                <div class="product-description product__card-desc">
                    
                                                    <p class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/35-124-polo.html#/1-taille-s/11-couleur-noir">Polo</a></p>
                                            
                    
                        


                    
                    
                                                    <div class="product-price-and-shipping text-center">
                                
                                

                                <span class="sr-only">Prix</span>
                                <span class="price">15,73 €</span>


                                

                                

                                <form class="pl_atc mt-2" action="http://localhost/prestashop/fr/panier" method="post">
                                   <input type="hidden" name="token" value="c52f0980ee6add53ed1c1d8408b340f7">
                                   <input type="hidden" value="35" name="id_product">
                                   <input type="number" class="input-group form-control" name="qty" value="1">
                                   <button data-button-action="add-to-cart"  class="btn btn-primary">Ajouter au panier</button>
                                </form>
                            </div>
                                            


                </div>

            </div>
                        
                <ul class="product-flags">
                                                        </ul>
            
        </div>
        


    </article>

          
    <article class="product-miniature js-product-miniature mb-3" data-id-product="30" data-id-product-attribute="75">
        <div class="card card-product">

            <div class="card-img-top product__card-img">
                
                    <a href="http://localhost/prestashop/fr/accueil/30-75-casquette.html#/11-couleur-noir" class="thumbnail product-thumbnail rc ratio1_1">
                                                    <img
                                    data-src = "http://localhost/prestashop/50-home_default/casquette.jpg"
                                    alt = "Casquette"
                                    data-full-size-image-url = "http://localhost/prestashop/50-large_default/casquette.jpg"
                                    class="lazyload"
                            >
                                            </a>
                
                <div class="highlighted-informations text-center p-2 visible--desktop">
                    
                        <span class="quick-view" data-link-action="quickview">
                      <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
                  </span>
                    

                    
                                                    <div class="variant-links">
                        <a href="http://localhost/prestashop/fr/accueil/30-74-casquette.html#/8-couleur-blanc"
               class="color mx-1"
               title="Blanc"
                                         style="background-color: #ffffff"                                 ><span class="sr-only">Blanc</span></a>
                                <a href="http://localhost/prestashop/fr/accueil/30-75-casquette.html#/11-couleur-noir"
               class="color mx-1"
               title="Noir"
                                         style="background-color: #434A54"                                 ><span class="sr-only">Noir</span></a>
                </div>
                                            
                </div>
            </div>
            
            <div class="card-body">
                <div class="product-description product__card-desc">
                    
                                                    <p class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/30-75-casquette.html#/11-couleur-noir">Casquette</a></p>
                                            
                    
                        


                    
                    
                                                    <div class="product-price-and-shipping text-center">
                                
                                

                                <span class="sr-only">Prix</span>
                                <span class="price">10,89 €</span>


                                

                                

                                <form class="pl_atc mt-2" action="http://localhost/prestashop/fr/panier" method="post">
                                   <input type="hidden" name="token" value="c52f0980ee6add53ed1c1d8408b340f7">
                                   <input type="hidden" value="30" name="id_product">
                                   <input type="number" class="input-group form-control" name="qty" value="1">
                                   <button data-button-action="add-to-cart"  class="btn btn-primary">Ajouter au panier</button>
                                </form>
                            </div>
                                            


                </div>

            </div>
                        
                <ul class="product-flags">
                                                        </ul>
            
        </div>
        


    </article>

          
    <article class="product-miniature js-product-miniature mb-3" data-id-product="34" data-id-product-attribute="116">
        <div class="card card-product">

            <div class="card-img-top product__card-img">
                
                    <a href="http://localhost/prestashop/fr/accueil/34-116-t-shirt.html#/1-taille-s/11-couleur-noir" class="thumbnail product-thumbnail rc ratio1_1">
                                                    <img
                                    data-src = "http://localhost/prestashop/74-home_default/t-shirt.jpg"
                                    alt = "T-shirt"
                                    data-full-size-image-url = "http://localhost/prestashop/74-large_default/t-shirt.jpg"
                                    class="lazyload"
                            >
                                            </a>
                
                <div class="highlighted-informations text-center p-2 visible--desktop">
                    
                        <span class="quick-view" data-link-action="quickview">
                      <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
                  </span>
                    

                    
                                                    <div class="variant-links">
                        <a href="http://localhost/prestashop/fr/accueil/34-112-t-shirt.html#/1-taille-s/8-couleur-blanc"
               class="color mx-1"
               title="Blanc"
                                         style="background-color: #ffffff"                                 ><span class="sr-only">Blanc</span></a>
                                <a href="http://localhost/prestashop/fr/accueil/34-116-t-shirt.html#/1-taille-s/11-couleur-noir"
               class="color mx-1"
               title="Noir"
                                         style="background-color: #434A54"                                 ><span class="sr-only">Noir</span></a>
                </div>
                                            
                </div>
            </div>
            
            <div class="card-body">
                <div class="product-description product__card-desc">
                    
                                                    <p class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/34-116-t-shirt.html#/1-taille-s/11-couleur-noir">T-shirt</a></p>
                                            
                    
                        


                    
                    
                                                    <div class="product-price-and-shipping text-center">
                                
                                

                                <span class="sr-only">Prix</span>
                                <span class="price">14,52 €</span>


                                

                                

                                <form class="pl_atc mt-2" action="http://localhost/prestashop/fr/panier" method="post">
                                   <input type="hidden" name="token" value="c52f0980ee6add53ed1c1d8408b340f7">
                                   <input type="hidden" value="34" name="id_product">
                                   <input type="number" class="input-group form-control" name="qty" value="1">
                                   <button data-button-action="add-to-cart"  class="btn btn-primary">Ajouter au panier</button>
                                </form>
                            </div>
                                            


                </div>

            </div>
                        
                <ul class="product-flags">
                                                        </ul>
            
        </div>
        


    </article>

          
    <article class="product-miniature js-product-miniature mb-3" data-id-product="36" data-id-product-attribute="132">
        <div class="card card-product">

            <div class="card-img-top product__card-img">
                
                    <a href="http://localhost/prestashop/fr/accueil/36-132-jogging.html#/1-taille-s/11-couleur-noir" class="thumbnail product-thumbnail rc ratio1_1">
                                                    <img
                                    data-src = "http://localhost/prestashop/82-home_default/jogging.jpg"
                                    alt = "Jogging"
                                    data-full-size-image-url = "http://localhost/prestashop/82-large_default/jogging.jpg"
                                    class="lazyload"
                            >
                                            </a>
                
                <div class="highlighted-informations text-center p-2 visible--desktop">
                    
                        <span class="quick-view" data-link-action="quickview">
                      <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
                  </span>
                    

                    
                                                    <div class="variant-links">
                        <a href="http://localhost/prestashop/fr/accueil/36-128-jogging.html#/1-taille-s/8-couleur-blanc"
               class="color mx-1"
               title="Blanc"
                                         style="background-color: #ffffff"                                 ><span class="sr-only">Blanc</span></a>
                                <a href="http://localhost/prestashop/fr/accueil/36-132-jogging.html#/1-taille-s/11-couleur-noir"
               class="color mx-1"
               title="Noir"
                                         style="background-color: #434A54"                                 ><span class="sr-only">Noir</span></a>
                </div>
                                            
                </div>
            </div>
            
            <div class="card-body">
                <div class="product-description product__card-desc">
                    
                                                    <p class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/36-132-jogging.html#/1-taille-s/11-couleur-noir">Jogging</a></p>
                                            
                    
                        


                    
                    
                                                    <div class="product-price-and-shipping text-center">
                                
                                

                                <span class="sr-only">Prix</span>
                                <span class="price">20,57 €</span>


                                

                                

                                <form class="pl_atc mt-2" action="http://localhost/prestashop/fr/panier" method="post">
                                   <input type="hidden" name="token" value="c52f0980ee6add53ed1c1d8408b340f7">
                                   <input type="hidden" value="36" name="id_product">
                                   <input type="number" class="input-group form-control" name="qty" value="1">
                                   <button data-button-action="add-to-cart"  class="btn btn-primary">Ajouter au panier</button>
                                </form>
                            </div>
                                            


                </div>

            </div>
                        
                <ul class="product-flags">
                                                        </ul>
            
        </div>
        


    </article>

        </div>
  <a class="all-product-link float-left float-md-right" href="http://localhost/prestashop/fr/2-accueil">
    Tous les produits<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
